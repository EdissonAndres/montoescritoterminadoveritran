package prueba.veritran.net.pruebauniversidad;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UniversityUnitTest {
    @Test
    public void ceroTest() { assertEquals( "cero", MontoEscrito.monto(0));
    }

    @Test
    public void nueveTest() {
        assertEquals( "nueve", MontoEscrito.monto(9));
    }

    @Test
    public void milTest() {
        assertEquals("mil", MontoEscrito.monto(1000));
    }

    @Test
    public void mil_1_Test() { assertEquals("nueve mil ciento cincuenta y seis", MontoEscrito.monto(9156));
    }

    @Test
    public void millonTest() {
        assertEquals("un millon", MontoEscrito.monto(1000000));
    }

    @Test
    public void millon_1_Test() { assertEquals( "tres millones doscientos noventa mil seiscientos cuarenta y uno", MontoEscrito.monto(3290641));
    }

    @Test
    public void millon_2_Test() { assertEquals( "un millon veintisiete mil cuatrocientos diez", MontoEscrito.monto(1027410));
    }
}